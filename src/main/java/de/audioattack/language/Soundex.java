/*
 * Copyright 2013 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.language;

import java.util.Locale;
import java.util.Map;

/**
 * Implementation of American Soundex algorithm for different languages.
 *
 * @author Marc Nause <marc.nause@gmx.de>
 * @see <a href="http://en.wikipedia.org/wiki/Soundex">http://en.wikipedia.org/wiki/Soundex</a>
 */
public final class Soundex {

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private Soundex() {
    }

    /**
     * Calculates Soundex code for a given word in a given language.
     *
     * @param language the language
     * @param word     the word, may only contain letters, must not be {@code null} or empty
     * @return Soundex code of the word
     */
    public static String getSoundexCode(final Language language, final String word) {
        if (word == null || !word.matches("^\\p{L}+$")) {
            throw new IllegalArgumentException("Word may only contain letters, must not be <null> or empty: '" + word + "'");
        }

        final Map<Character, Integer> map = language.getMap();
        final String wordToLower = word.toLowerCase(Locale.ROOT);
        final StringBuilder sb = new StringBuilder();

        char currentChar;
        Integer currentNumber;
        Integer lastNumber = null;
        for (int i = 0, len = wordToLower.length(); i < len && sb.length() < 4; i++) {
            currentChar = wordToLower.charAt(i);
            if (sb.length() == 0) {
                sb.append(Character.toUpperCase(currentChar));
                lastNumber = map.get(currentChar);
            } else {
                currentNumber = map.get(currentChar);
                if (currentNumber != null
                        && (lastNumber == null
                        || currentNumber.intValue() != lastNumber.intValue())) {
                    sb.append(currentNumber.toString());
                    lastNumber = currentNumber;
                } else if (currentChar != 'h'
                        && currentChar != 'w') {
                    lastNumber = null;
                }
            }
        }

        while (sb.length() < 4) {
            sb.append('0');
        }

        return sb.toString();
    }

}
