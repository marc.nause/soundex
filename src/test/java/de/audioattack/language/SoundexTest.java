package de.audioattack.language;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SoundexTest {

    @Test
    void testIllegalInput() {
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.ENGLISH, "123"));
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.ENGLISH, "(test)"));
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.ENGLISH, ""));
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.ENGLISH, null));
    }

    @Test
    void testEnglish() {
        // Examples from https://en.wikipedia.org/wiki/Soundex
        assertEquals("R163", Soundex.getSoundexCode(Language.ENGLISH, "Robert"));
        assertEquals("R163", Soundex.getSoundexCode(Language.ENGLISH, "Rupert"));
        assertEquals("R150", Soundex.getSoundexCode(Language.ENGLISH, "Rubin"));
        assertEquals("A261", Soundex.getSoundexCode(Language.ENGLISH, "Ashcraft"));
        assertEquals("A261", Soundex.getSoundexCode(Language.ENGLISH, "Ashcroft"));
        assertEquals("T522", Soundex.getSoundexCode(Language.ENGLISH, "Tymczak"));
        assertEquals("P236", Soundex.getSoundexCode(Language.ENGLISH, "Pfister"));

        // Examples from https://web.stanford.edu/class/archive/cs/cs106b/cs106b.1208/assignments/assign1/soundex
        assertEquals("J000", Soundex.getSoundexCode(Language.ENGLISH, "Jue"));
        assertEquals("M236", Soundex.getSoundexCode(Language.ENGLISH, "Master"));

        // Examples from https://gist.github.com/andersivner/4024659
        assertEquals("A000", Soundex.getSoundexCode(Language.ENGLISH, "a"));
        assertEquals("I000", Soundex.getSoundexCode(Language.ENGLISH, "i"));
        assertEquals("A100", Soundex.getSoundexCode(Language.ENGLISH, "Ab"));
        assertEquals("A200", Soundex.getSoundexCode(Language.ENGLISH, "Ac"));
        assertEquals("A340", Soundex.getSoundexCode(Language.ENGLISH, "Adl"));
        assertEquals("A256", Soundex.getSoundexCode(Language.ENGLISH, "Ajmr"));
        assertEquals("D123", Soundex.getSoundexCode(Language.ENGLISH, "Dbcdlmr"));
        assertEquals("C123", Soundex.getSoundexCode(Language.ENGLISH, "CAaEeIiOoUuHhYybcd"));
        assertEquals("G123", Soundex.getSoundexCode(Language.ENGLISH, "Gbfcgdt"));
        assertEquals("A123", Soundex.getSoundexCode(Language.ENGLISH, "abcd"));
        assertEquals("B234", Soundex.getSoundexCode(Language.ENGLISH, "BCDL"));
        assertEquals("J110", Soundex.getSoundexCode(Language.ENGLISH, "Jbob"));
        assertEquals("B230", Soundex.getSoundexCode(Language.ENGLISH, "Bbcd"));
        assertEquals("J100", Soundex.getSoundexCode(Language.ENGLISH, "Jbwb"));

        // Examples from https://stephenhaunts.com/2014/01/17/phonetic-string-matching-soundex/
        assertEquals("S315", Soundex.getSoundexCode(Language.ENGLISH, "Stephen"));
        assertEquals("S315", Soundex.getSoundexCode(Language.ENGLISH, "Steven"));
        assertEquals("R163", Soundex.getSoundexCode(Language.ENGLISH, "Robert"));
        assertEquals("R163", Soundex.getSoundexCode(Language.ENGLISH, "Rupert"));
    }

    @Test
    void testGerman() {
        assertEquals("H530", Soundex.getSoundexCode(Language.GERMAN, "Hund"));
        assertEquals("H530", Soundex.getSoundexCode(Language.GERMAN, "Hand"));
        assertEquals("P100", Soundex.getSoundexCode(Language.GERMAN, "Pfeife"));
        assertEquals("P100", Soundex.getSoundexCode(Language.GERMAN, "Pappe"));
        assertEquals("S162", Soundex.getSoundexCode(Language.GERMAN, "Superzicke"));
        assertEquals("S162", Soundex.getSoundexCode(Language.GERMAN, "Spears"));
        assertEquals("A500", Soundex.getSoundexCode(Language.GERMAN, "Anna"));
        assertEquals("A500", Soundex.getSoundexCode(Language.GERMAN, "Anne"));
        assertEquals("A000", Soundex.getSoundexCode(Language.GERMAN, "a"));
    }

    @Test
    void testException() {
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.ENGLISH, "a b"));
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.GERMAN, "a!b"));
        assertThrows(IllegalArgumentException.class, () -> Soundex.getSoundexCode(Language.FRENCH, "a1b"));
        assertDoesNotThrow(() -> Soundex.getSoundexCode(Language.ENGLISH, "ab"));
    }


}
