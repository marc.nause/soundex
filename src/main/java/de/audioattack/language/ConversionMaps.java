/*
 * Copyright 2013 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.language;

import java.util.HashMap;
import java.util.Map;

/**
 * Conversion maps for different languages.
 *
 * @author Marc Nause <marc.nause@gmx.de>
 */
final class ConversionMaps {

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private ConversionMaps() {
    }

    /**
     * Conversion map for English language.
     */
    static final Map<Character, Integer> EN_MAP = new HashMap<>();

    static {
        EN_MAP.put('b', 1);
        EN_MAP.put('f', 1);
        EN_MAP.put('p', 1);
        EN_MAP.put('v', 1);
        EN_MAP.put('c', 2);
        EN_MAP.put('g', 2);
        EN_MAP.put('j', 2);
        EN_MAP.put('k', 2);
        EN_MAP.put('q', 2);
        EN_MAP.put('s', 2);
        EN_MAP.put('x', 2);
        EN_MAP.put('z', 2);
        EN_MAP.put('d', 3);
        EN_MAP.put('t', 3);
        EN_MAP.put('l', 4);
        EN_MAP.put('m', 5);
        EN_MAP.put('n', 5);
        EN_MAP.put('r', 6);
    }

    /**
     * Conversion map for German language. According to
     * <a href="https://web.archive.org/web/20180709154422/https://www.groupxs.com/archives/anpassung-des-soundex-algorithmus-fur-die-deutsche-sprache/132.html">
     * Anpassung des Soundex-Algorithmus für die deutsche Sprache</a> without the 'ch' since it does not fit our algorithm.
     */
    static final Map<Character, Integer> DE_MAP = new HashMap<>();

    static {
        DE_MAP.put('b', 1);
        DE_MAP.put('p', 1);
        DE_MAP.put('f', 1);
        DE_MAP.put('v', 1);
        DE_MAP.put('w', 1);
        DE_MAP.put('c', 2);
        DE_MAP.put('g', 2);
        DE_MAP.put('k', 2);
        DE_MAP.put('q', 2);
        DE_MAP.put('x', 2);
        DE_MAP.put('s', 2);
        DE_MAP.put('z', 2);
        DE_MAP.put('ß', 2);
        DE_MAP.put('d', 3);
        DE_MAP.put('t', 3);
        DE_MAP.put('l', 4);
        DE_MAP.put('m', 5);
        DE_MAP.put('n', 5);
        DE_MAP.put('r', 6);
    }

    /**
     * Conversion map for French language. According to <a href="https://fr.wikipedia.org/wiki/Soundex">French Wikipedia</a>.
     */
    static final Map<Character, Integer> FR_MAP = new HashMap<>();

    static {
        FR_MAP.put('b', 1);
        FR_MAP.put('p', 1);
        FR_MAP.put('c', 2);
        FR_MAP.put('k', 2);
        FR_MAP.put('q', 2);
        FR_MAP.put('d', 3);
        FR_MAP.put('t', 3);
        FR_MAP.put('l', 4);
        FR_MAP.put('m', 5);
        FR_MAP.put('n', 5);
        FR_MAP.put('r', 6);
        FR_MAP.put('g', 7);
        FR_MAP.put('j', 7);
        FR_MAP.put('x', 8);
        FR_MAP.put('z', 8);
        FR_MAP.put('s', 8);
        FR_MAP.put('f', 9);
        FR_MAP.put('v', 9);
    }

}
