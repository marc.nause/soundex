/*
 * Copyright 2013 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.language;

import java.util.Map;

/**
 * Supported languages. If your language is not supported, try
 * {@link Language#ENGLISH}.
 *
 * @author Marc Nause <marc.nause@gmx.de>
 */
public enum Language {

    /**
     * English language.
     */
    ENGLISH(ConversionMaps.EN_MAP),
    /**
     * French language.
     */
    FRENCH(ConversionMaps.FR_MAP),
    /**
     * German language.
     */
    GERMAN(ConversionMaps.DE_MAP);

    /**
     * Conversion map of the language.
     */
    private final Map<Character, Integer> map;

    /**
     * Constructor.
     *
     * @param map conversion map of the language
     */
    Language(final Map<Character, Integer> map) {
        this.map = map;
    }

    /**
     * Gets conversion map of a language.
     *
     * @return the conversion map
     */
    Map<Character, Integer> getMap() {
        return map;
    }

}
